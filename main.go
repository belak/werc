package main

import (
	"fmt"
	"github.com/russross/blackfriday"
	"html/template"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// Global base dir - where the files are located
var basedir = "./"

type TemplateData struct {
	Title        string
	SiteTitle    string
	SiteSubtitle string
	Dir          []MenuItem
	URL          *url.URL
}

type TimedHandler func(w http.ResponseWriter, r *http.Request)

func (h TimedHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	h(w, r)
	fmt.Println("Took", time.Since(start), "to handle", r.Host+r.URL.Path)
}

func staticHandle(w http.ResponseWriter, r *http.Request) {
	host, _, err := net.SplitHostPort(r.Host)
	if err != nil {
		host = r.Host
	}

	file_dir := filepath.Join(basedir, "sites", host, "_werc")
	file_name := filepath.Join(file_dir, r.URL.Path)
	stat, err := os.Stat(file_name)
	if err == nil && !stat.IsDir() {
		http.FileServer(http.Dir(file_dir)).ServeHTTP(w, r)
	} else {
		siteHandle(w, r)
	}
}

func siteHandle(w http.ResponseWriter, r *http.Request) {
	rep := strings.NewReplacer(string(os.PathSeparator), " - ")
	host, _, err := net.SplitHostPort(r.Host)
	if err != nil {
		host = r.Host
	}
	host_dir := filepath.Join(basedir, "sites", host)
	tpl_dir := filepath.Join(host_dir, "_werc", "tpl")
	file_name := filepath.Join(host_dir, r.URL.Path)
	selected := r.URL.Path

	// Copy over so we can work on it a bit...
	// Here we drop off the leading slash
	title := selected[1:]
	if len(title) > 0 && title[len(title)-1] == os.PathSeparator {
		title = title[:len(title)-1]
	}

	data := TemplateData{Title: rep.Replace(title), SiteTitle: host, URL: r.URL}

	// TODO: Figure out why this is actually happening
	// Strange fix right here... a few fields aren't set in the r.URL, so we set them manually
	data.URL.Host = r.Host
	data.URL.Scheme = "http"

	tpl_files, err := filepath.Glob(filepath.Join(tpl_dir, "*.tpl"))
	if err != nil {
		fmt.Println(err)
		return
	}

	t, err := template.New("Site Templates").ParseFiles(tpl_files...)
	if err != nil {
		fmt.Println(err)
		return
	}

	// If they want a dir, give them the index
	_, err = os.Stat(file_name + ".md")
	if os.IsNotExist(err) {
		selected = selected + "/"
	}

	if strings.HasSuffix(selected, "/") {
		stat, err := os.Stat(file_name)
		if err == nil && stat.IsDir() {
			file_name = filepath.Join(file_name, "index")
		}
	}

	file_name = file_name + ".md"

	file, err := ioutil.ReadFile(file_name)
	var content []byte
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		content = blackfriday.MarkdownCommon(file)
	}

	// Throw in some sane defaults
	data.Dir = MenuItem{
		Path:     "/",
		Title:    "",
		IsDir:    true,
		Scanned:  false,
		prefix:   host_dir,
		selected: selected,
	}.Children()

	t.Lookup("head").Execute(w, data)
	if len(content) > 0 {
		w.Write(content)
	} else {
		t.Lookup("404").Execute(w, data)
	}
	t.Lookup("foot").Execute(w, data)
}

func main() {
	fmt.Println("Starting http Server...")

	http.Handle("/pub/", TimedHandler(staticHandle))
	http.Handle("/", TimedHandler(siteHandle))

	err := http.ListenAndServe("127.0.0.1:11235", nil)
	if err != nil {
		fmt.Printf("ListenAndServe Error :" + err.Error())
		return
	}
}

type MenuItem struct {
	Path     string
	Title    string
	IsDir    bool
	Scanned  bool
	prefix   string
	selected string
	children []MenuItem
}

func (m MenuItem) Selected() bool {
	return strings.HasPrefix(m.selected, m.Path)
}

func (m MenuItem) HasChildren() bool {
	// Be sure to scan
	return len(m.Children()) > 0
}

func (m MenuItem) Children() []MenuItem {
	if m.Scanned {
		return m.children
	}

	e, err := filepath.Glob(filepath.Join(m.prefix, m.Path, "*"))
	if err != nil {
		fmt.Println(err)
	}

	for _, j := range e {
		info, _ := os.Stat(j)
		if j[len(m.prefix)+1] != '.' {
			if info.IsDir() {
				if j[len(m.prefix)+1] != '_' {
					n := MenuItem{
						Path:     j[len(m.prefix):] + string(os.PathSeparator),
						Title:    filepath.Base(j[len(m.prefix)+1:]) + string(os.PathSeparator),
						IsDir:    true,
						Scanned:  false,
						prefix:   m.prefix,
						selected: m.selected,
					}
					m.children = append(m.children, n)
				}
			} else {
				ext := filepath.Ext(j)
				base := filepath.Base(j)
				if len(ext) != -1 {
					base = base[:len(base)-len(ext)]
				}
				if base != "index" {
					switch ext {
					case ".md":
						n := MenuItem{
							Path:     j[len(m.prefix) : len(j)-3],
							Title:    filepath.Base(j[len(m.prefix)+1 : len(j)-3]),
							IsDir:    true,
							Scanned:  false,
							prefix:   m.prefix,
							selected: m.selected,
						}
						m.children = append(m.children, n)
					}
				}
			}
		}
	}

	m.Scanned = true

	if len(m.children) == 0 {
		return []MenuItem{}
	}

	return m.children
}
