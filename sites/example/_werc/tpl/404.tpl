{{define "404"}}
<h1>The requested document at '{{.URL.Scheme}}://{{.URL.Host}}{{.URL.Path}}' doesn't exist.</h1>

{{end}}
