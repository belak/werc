{{define "side"}}
{{if .}}
<ul>
{{range .}}
	{{if .IsDir}}
		{{if .Selected}}
			<li><a href="{{.Path}}" class="current"><i>{{.Title}}</i></a></li>
			{{if .HasChildren}}
				<li>{{template "side" .Children}}</li>
			{{end}}
		{{else}}
			<li><a href="{{.Path}}">{{.Title}}</a></li>
		{{end}}
	{{else}}
		<li><a href="{{.Path}}">{{.Title}}</a></li>
	{{end}}
{{end}}
</ul>
{{end}}
{{end}}
