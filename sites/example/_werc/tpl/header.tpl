{{define "head"}}<!DOCTYPE html>
<html>
<head>
	<title>{{if .Title}}{{.SiteTitle}} - {{.Title}}{{else}}{{.SiteTitle}}{{end}}</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/pub/css/style.css" type="text/css" media="screen" title="default">
	<link rel="stylesheet" href="/pub/css/highlight.css" type="text/css" media="screen" title="default">
	<script type="text/javascript" src="/pub/js/highlight.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
<div id="main">
	<div id="head">
		<div class="left">
			<a href="http://belak.so">belak.so</a> |
			<a href="http://git.belak.so">git</a>
		</div>
	</div>

	<div id="menu">
{{template "side" .Dir}}
	</div>

	<div id="body">
		<div id="content">
{{end}}
